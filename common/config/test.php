<?php

use \yii\helpers\Url;

return [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
        ],
        'mailer' => [
            'useFileTransport' => true
        ],
    ],
];